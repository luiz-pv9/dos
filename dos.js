// dependencies
var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    register = require('./register.js'),
    config = require('./config.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser.multipart());

// CORS
app.use( function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});

// uploading route to track events
app.post('/dos', function(req, res) {
    var data = req.body.data;
    if('string' === typeof data) {
        data = JSON.parse(data);
    }
    register(data);
    res.end("OK");
});

app.listen(config.server.port, function() {
    console.log("server running on port", config.server.port);
});