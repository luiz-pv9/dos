var request = require('request'),
    config = require('./config.js'),
    querystring = require('querystring');

var registerEvents = function(user, events) {
    function registerEvent(event) {
        event['event_type'] = event['name'];
        event['external_user_id'] = user;
        delete event['name'];

        request.post({
            url: 'http://' + config.eventhub.host + ":" + config.eventhub.port +
                '/events/track',
            body: querystring.stringify(event)
        }, function(err, response, body) {
            events.shift();
            console.log(events.length + " events to go");
            if(events.length > 0) {
                registerEvent(events[0]);
            }
        });
    }
    registerEvent(events[0]);
};

module.exports = exports = function(data) {
    var user = data['user_id'];
    console.log("BEGIN: Tracking events");
    return registerEvents(user, data['events']);
};